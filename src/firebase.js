import firebase from "firebase/app";
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyByZADNM9L8uuNQ4O1c_ZTFuKnC9Co2og8",
    authDomain: "cursoreactredux-pokeauth.firebaseapp.com",
    projectId: "cursoreactredux-pokeauth",
    storageBucket: "cursoreactredux-pokeauth.appspot.com",
    messagingSenderId: "1099387464320",
    appId: "1:1099387464320:web:af26a9ff6f09d413cb1465"
  };
  
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth()

export {auth, firebase}
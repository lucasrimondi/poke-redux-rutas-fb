import React, {} from 'react'

import {useSelector} from 'react-redux'

const Detalle = () => {


    const unPokemon = useSelector(store => store.pokemones.unPokemon)


    return unPokemon ? ( //si unPokemon existe, que nos lo devuelva, es porque a veces tarda en hacer la peticion
        <div className='card mt-4 text-center'>
            <div className="card-body">
                <img src={unPokemon.img} alt="" className="img-fluid" />
                <div className="card-title text-uppercase">{unPokemon.nombre}</div>
                <p className="card-text">Height: {unPokemon.height} - Weight: {unPokemon.weight}</p>
            </div>
        </div>
    ) : null 
}

export default Detalle

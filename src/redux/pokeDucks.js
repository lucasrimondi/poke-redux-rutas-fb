import axios from 'axios'


//CONSTANTS
const dataInicial = {
    count: 0,
    next: null,
    previous: null,
    results: []
} //Estos datos son lo que te trae la llamada a la api, inicio el state con todo vacio 

//types
const OBTENER_POKEMONES_EXITO   = 'OBTENER_POKEMONES_EXITO'
const SIGUIENTE_POKEMONES_EXITO = 'SIGUIENTE_POKEMONES_EXITO'
const ANTERIOR_POKEMONES_EXITO  = 'ANTERIOR_POKEMONES_EXITO'
const DETALLES_POKEMONES_EXITO  = 'DETALLES_POKEMONES_EXITO'


//REDUCER
export default function pokeReducer(state = dataInicial, action) {

    switch(action.type){
        case OBTENER_POKEMONES_EXITO:
            return {...state, ...action.payload}
        case SIGUIENTE_POKEMONES_EXITO:
            return {...state, ...action.payload}
        case ANTERIOR_POKEMONES_EXITO:
            return {...state, ...action.payload}
        case DETALLES_POKEMONES_EXITO:
            return {...state, unPokemon: action.payload} //guardo toda esta info nueva en otro objeto asi no enquilombo la data inicial con muchos datos mas
        default:
            return state
    }
}




//ACTIONS//

export const obtenerPokemonesAccion = () => async(dispatch) => {

    if(localStorage.getItem('offset=0')){
        dispatch({
            type: OBTENER_POKEMONES_EXITO,
            payload: JSON.parse(localStorage.getItem('offset=0'))
        })
        return //con este return sale de la funcion
    } //REVISA ANTES DE LLAMAR A LA API SI YO YA TENGO GUARDADA LA INFO EL LOCAL STORAGE


    try{
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=0&limit=20`)
        dispatch({
            type: OBTENER_POKEMONES_EXITO,
            payload: res.data
        })

        localStorage.setItem('offset=0', JSON.stringify(res.data))
        //CONSUME DE LA API Y ME LO GUARDA EN EL STORAGE

    } catch (error){

    }
}

export const siguientePokeAction = () => async (dispatch, getState) => {

    const next = getState().pokemones.next

    if(localStorage.getItem(next)){
        dispatch({
            type: SIGUIENTE_POKEMONES_EXITO,
            payload: JSON.parse(localStorage.getItem(next))
        })
        return //con este return sale de la funcion
    } //REVISA ANTES DE LLAMAR A LA API SI YO YA TENGO GUARDADA LA INFO EL LOCAL STORAGE


    try{
        const res = await axios.get(next)
        dispatch({
            type: SIGUIENTE_POKEMONES_EXITO,
            payload: res.data
        })

        localStorage.setItem(next, JSON.stringify(res.data))

    } catch (error){

    }
}


export const anteriorPokeAction = () => async (dispatch, getState) => {
    
    const previous = getState().pokemones.previous

    if(localStorage.getItem(previous)){
        dispatch({
            type: SIGUIENTE_POKEMONES_EXITO,
            payload: JSON.parse(localStorage.getItem(previous))
        })
        return //con este return sale de la funcion
    } //REVISA ANTES DE LLAMAR A LA API SI YO YA TENGO GUARDADA LA INFO EL LOCAL STORAGE

    try{
        const res = await axios.get(previous)
        dispatch({
            type: ANTERIOR_POKEMONES_EXITO,
            payload: res.data
        })

        localStorage.setItem(previous, JSON.stringify(res.data))

    } catch (error){

    }
}


export const pokeDetalleAction = (url) => async (dispatch, getState) => {

    try{
        const res = await axios.get(url)

        dispatch({
            type: DETALLES_POKEMONES_EXITO,
            payload: {
                nombre: res.data.name,
                height: res.data.height,
                weight: res.data.weight,
                img: res.data.sprites.front_default
            }
        })


    } catch (error){

    }
}
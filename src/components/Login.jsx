import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {ingresoUsuarioAction} from '../redux/userDuck'

import { useNavigate } from 'react-router-dom'

const Login = () => {

    const dispatch = useDispatch()

    const loading = useSelector(store => store.usuario.loading)
    const active  = useSelector(store => store.usuario.activo)

    const navigate = useNavigate()

    useEffect(() => {
        if(active){
            navigate('/')
        }
    }, [navigate, active])


    return (
        <div className="mt-5 text-center">
            <h3>Ingreso de usuarios</h3>
            <hr/>
                 <button 
                    className="btn btn-dark"
                    onClick={()=>dispatch(ingresoUsuarioAction())}
                    disabled={loading}>
                    Google
                 </button>
        </div>
    )
}

export default Login
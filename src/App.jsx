import Pokemones from "./components/Pokemones";
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Login from './components/Login'
import Navbar from './components/Navbar'

function App() {

  return (
    <BrowserRouter>
      <div className="container mt-3">
        <Navbar />
        
        <Routes>
          <Route path="/" element={<Pokemones />}></Route>
          <Route path="login" element={<Login />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

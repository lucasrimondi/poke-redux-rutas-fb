import {auth, firebase} from '../firebase'



//data inicial
const dataInicial = {
    loading: false,
    activo: false
}


//type
const LOADING = 'LOADING'
const USER_ERROR = 'USER_ERROR'
const USER_EXITO = 'USER_EXITO'
const CERRAR_SESION = 'CERRAR_SESION'



//REDUCER
export default function userReducer (state = dataInicial, action){
    switch(action.type){
        case LOADING:
            return {...state, loading: true}
        case USER_ERROR:
            return {...dataInicial}
        case USER_EXITO:
            return {...state, loading: false, user: action.payload, activo: true}
        case CERRAR_SESION:
            return {...dataInicial}
        default:
            return {...state}
    }

}

//ACTIONS

export const ingresoUsuarioAction = () => async (dispatch) => {
    dispatch({
        type: LOADING
    }) //que apenas apretas la accion y mientras esta cargando y accediendo al popup de acceso, estamos en modo loading
    try{
        const provider = new firebase.auth.GoogleAuthProvider()

        const res = await auth.signInWithPopup(provider)


        dispatch({
            type: USER_EXITO,
            payload: {
                uid: res.user.uid,
                email: res.user.email
            }
        })
        localStorage.setItem('usuario', JSON.stringify({
            uid: res.user.uid,
            email: res.user.email
        }))


    } catch (error){
        dispatch({
            type: USER_ERROR
        })

    }
}

export const leerUsuarioActivoAction = () => (dispatch) => {
    if(localStorage.getItem('usuario')){
        dispatch({
            type: USER_EXITO,
            payload: JSON.parse(localStorage.getItem('usuario'))
        })
    }
    }


export const cerrarSesiónAction = () => (dispatch) => {
    auth.signOut()
    localStorage.removeItem('usuario')
    dispatch({
        type: CERRAR_SESION
    })
}

import React from 'react'
import {Link, NavLink, useNavigate} from 'react-router-dom'

import { useDispatch, useSelector } from 'react-redux'
import {cerrarSesiónAction} from '../redux/userDuck'


const Navbar = () => {

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const cerrarSesion = () => {
        dispatch(cerrarSesiónAction())
        navigate('/login')
    }

    const active  = useSelector(store => store.usuario.activo)


    return (
        <div className="navbar navbar-dark bg-dark container-fluid">
            <Link to="/" className="navbar-brand">Poke API</Link>
            <div>
                <div className="d-flex">
                    {
                        active ? (
                                <>
                                <NavLink 
                                    className="btn btn-dark mr-2" 
                                    to="/">
                                    Pokemon
                                </NavLink>
                                <button
                                    className="btn btn-dark mr-2" 
                                    onClick={() => cerrarSesion()}>
                                Cerrar Sesión
                                </button>
                                </>
                                ) : (

                                <NavLink 
                                    className="btn btn-dark mr-2" 
                                    to="/login">
                                     Login
                                </NavLink>
                                )
                    }     
                </div>
            </div>
        </div>
    )
}

export default Navbar

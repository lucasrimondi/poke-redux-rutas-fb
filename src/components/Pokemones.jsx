import React, {useEffect} from 'react'

import {useDispatch, useSelector} from 'react-redux'
import {useNavigate} from 'react-router-dom'
import {obtenerPokemonesAccion, siguientePokeAction, anteriorPokeAction, pokeDetalleAction} from '../redux/pokeDucks'
import Detalle from './Detalle'


const Pokemones = () => {

    const dispatch  = useDispatch() //con este hook, despacho las acciones

    const pokemones = useSelector(store => store.pokemones.results)
    
    const next      = useSelector(store => store.pokemones.next)

    const previous  = useSelector(store => store.pokemones.previous)
    
    //con el useSelector traigo al componente los datos del store!! es fantastico!
    const active  = useSelector(store => store.usuario.activo)
    const navigate = useNavigate()

    useEffect(() => {
        if(!active){
            navigate('/login')
        }
    }, [navigate, active])

    return (
        <div className='row mt-4'>
            <div className="col-md-6">            

                    <h3>Lista de Pokemon</h3>
                    <ul className='list-group mt-4'>
                        {
                            pokemones.map((poke,index) => (
                                <li className="list-group-item text-uppercase" key={ index }> { poke.name }
                                    <button className="btn btn-dark btn-sm float-end" onClick={()=> dispatch(pokeDetalleAction(poke.url))}>Info</button>
                                </li>
                            ))
                        }
                    </ul>
                        <div className="d-flex justify-content-center my-4 mx-2">
                            {
                                pokemones.length === 0 && //si no hay nada en el array
                                <button className="btn btn-dark" onClick={()=> dispatch(obtenerPokemonesAccion())}>Get Pokemones</button>
                            }
                            {
                                previous &&  //si previous no es null
                                <button className="btn btn-dark mx-2" onClick={()=> dispatch(anteriorPokeAction())}>Anteriores (20)</button>
                            }
                            {
                                next &&  //si next no es null
                                <button className="btn btn-dark mx-2" onClick={()=> dispatch(siguientePokeAction())}>Siguientes (20)</button>
                            }
                            
                        </div> 
            </div>
            
            <div className="col-md-6">
                <h3>Detalles</h3>
               <Detalle />
            </div>
        </div>

    )
}

export default Pokemones

import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'
 
import pokeReducer from './pokeDucks'
import userReducer, {leerUsuarioActivoAction} from './userDuck'

 
const rootReducer = combineReducers({
    pokemones: pokeReducer,
    usuario: userReducer
})
 
export default function generateStore() {
    const store = createStore( rootReducer, composeWithDevTools( applyMiddleware(thunk) ) )
    leerUsuarioActivoAction()(store.dispatch) //CADA VEZ QUE SE INICIA NUESTRO STORE, VA A LEER SI EXISTE EL USUARIO EN EL STORAGE Y ME LO MANDA AL STATE
    return store
}